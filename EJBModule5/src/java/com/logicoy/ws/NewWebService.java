/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logicoy.ws;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;

/**
 *
 * @author david
 */
@WebService(serviceName = "NewWebService")
@Stateless()
public class NewWebService {

    public NewWebService() {
    //    System.out.println("New instance : " + this.hashCode());
    }
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            Logger.getLogger(NewWebService.class.getName()).log(Level.SEVERE, null, ex);
        }
    //    System.out.println("Hello ! " + this.hashCode());
        return "Hello " + txt + " !";
    }
}
