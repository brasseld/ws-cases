/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logicoy.ws;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

/**
 *
 * @author david
 */
@WebService(serviceName = "NewWebService2")
@Stateless()
public class NewWebService {

    private static com.logicoy.ws.client.NewWebService_Service service;

    static {
        try {
            service = new com.logicoy.ws.client.NewWebService_Service(
                    new URL("http://localhost:8080/NewWebService/NewWebService?wsdl"),
                    new QName("http://ws.logicoy.com/", "NewWebService"));
        } catch (MalformedURLException ex) {
            Logger.getLogger(NewWebService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private final com.logicoy.ws.client.NewWebService servicePort;

    public NewWebService() {
        servicePort = service.getNewWebServicePort();
        BindingProvider bindingProvider = (BindingProvider) servicePort;
        bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                "http://localhost:8080/NewWebService/NewWebService");
    }

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return servicePort.hello(txt);
    }
}
